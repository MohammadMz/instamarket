import React, { Component } from 'react';
import ProForm from '../../components/proForm/ProForm';
import {Button} from 'antd';

export class ProductForms extends Component {

    state= {
        products : null,
        isLoaded : false
    }

    componentDidMount(){
        this.setState({products:this.props.products,isLoaded:true});
    }




    render() {



        let allForms = null;

        if(this.state.isLoaded){
        allForms = this.state.products.map(item=><ProForm  key={item.ID} product={item}/>)

        }







        return (
            <div>
                 {allForms}
                 {this.state.isLoaded ? <div className='MyContainer text-left pb-20'> <Button type='primary' >افزودن به محصولات</Button>
                 </div>
                 
                 :null }
                
            </div>
        )
    }
}

export default ProductForms;
