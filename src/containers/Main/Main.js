import React, { Component } from 'react';
import './Main.css';

export class Main extends Component {
    render() {
        return (

            <React.Fragment>
                <div className='Header rtl'>
                    <p className='mainPar'>فروشگاه ساز سپهر، راهکار یکپارچه فروش
                    برای فروش اینترنتی
                     <br/>
                    <span className='subHeading '> دیگه فقط وب سایت کافی نیست!
                    </span>
                    </p>
                </div>
                <div className='MyContainer mx-auto rtl '>
                    <h1 className='mt-16 Head'>کافیست آدرس پیج اینستاگرام خود را وارد کنید تا ما محصولات شما را به صفحه اصلی سایتتان وارد کنیم.</h1>


                </div>

            </React.Fragment>
            
        )
    }
}

export default Main
