import React, { Component } from 'react';
import axios from 'axios';
import { Spin } from 'antd';
import ProductForms from '../ProductForms/ProductForms';

export class Data extends Component {



    state = {
        allPosts: [],
        isLoaded: false,
        productsAfteredit: [],
        productsUpdated: false
    }




    componentDidMount() {
        // calling the function for ajax call for getting data from the server
        this.loadPost();


    }

    componentDidUpdate(prevProps) {
        if (this.props.name !== prevProps.name) {
            this.loadPost();
        }
    }


    loadPost() {

        //generating the url from instagram id that has been taken from the user in the shop component

        const Id = this.props.name;
        let pageAddress = 'https://www.instagram.com//?__a=1';
        let pageArray = pageAddress.split('');
        pageArray.splice(26, 0, Id);
        pageAddress = pageArray.join('');

        axios.get(pageAddress)
            .then(res => {
                let user = res.data.graphql.user;
                let allPosts = user.edge_owner_to_timeline_media.edges;
                //console.log('allPosts', allPosts);
                this.setState({ allPosts: allPosts, isLoaded: true });

                //calling the function to create an object from data that has been recieved 
                this.createProduct()
            })
            .catch(er => alert('صفحه مورد نظر موجود نیست'));



    }



    createProduct() {

        // looping through the post and create an array of object with edited text and price


        let editedCaption;
        let allProducts = [];


        allProducts = this.state.allPosts.map(item => {
            editedCaption = this.findCaptionPrice(item.node.edge_media_to_caption.edges[0].node.text)
            return {
                img: item.node.display_url,
                caption: editedCaption.caption,
                price: editedCaption.price,
                ID: item.node.id,
                name :editedCaption.name ,
                isChosen :false

            }

        })
        this.setState({ productsAfteredit: allProducts, productsUpdated: true })

    }


    // this function edit the caption from the instagram post 
    findCaptionPrice(text) {
        let editedText1,editedText, hashReg, regPrice, regWord, price,emojiReg ,name;
        emojiReg =/(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g
        hashReg = /#[ا-ی\w\d\W]+/g;
        editedText1 = text.replace(hashReg, '');
        editedText = editedText1.replace(emojiReg,'');
        regPrice = /قیمت:[ \d]*/g
        regWord = regPrice.exec(text);
        if (regWord !== null) {
            price = regWord[0].replace('قیمت:', '')
            editedText = editedText.replace(regPrice, '');

        } else {
            price = 0;
        }

        name = editedText.split('');
        name = name.splice(0,75);
        name = name.join('');

        





        return {
            caption: editedText,
            price: price ,
            name : name
        }


    }


    render() {

        return (
            <div>

                {this.state.productsUpdated ? <ProductForms products={this.state.productsAfteredit}/> : 
                 <div className='text-center '>         
                 <Spin size='large'/>
                                </div>
                }                

                </div>

            
        )
    }
}


export default Data;
