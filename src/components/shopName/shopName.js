
//this component is supposed to give the id of the instagram page and pass it to the data component

import React ,{useState}from 'react';
import {useFormik} from 'formik';
import '../../containers/Data/data';
import './shopName.css';
import Data from '../../containers/Data/data';
 const ShopName =(props) =>{

    const [userInput,setUserInput]=useState('');
    const formik = useFormik({
        initialValues: {
          instagramID: ''
        }
        ,
        onSubmit: (values) => {
          setUserInput(values.instagramID);
          }
        ,
      });

        return<div>
                  <form onSubmit={formik.handleSubmit} className='rtl Form MyContainer my-4'>
                    
                    <label htmlFor='instagramID'>نام ID اینستاگرام:</label>
                    <input type='text'
                        name='instagramID'
                        id='instagramID'
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}

                        value={formik.values.instagramID}/>
                    <button type="submit" className='btnSubmit'>ثبت فروشگاه</button>
                  </form>

                {(userInput) ? <Data name={userInput}/> : null}          
    
                </div>

    }

export default ShopName;


