import React from 'react';
import './proForm.css';
import { Form, Input, Checkbox , Row ,Col, InputNumber } from 'antd';


        const layout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 20,
        },
        };
        const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
        };
            
        const proForm = (props) => {


            








            const onFinish = values => {
                //console.log('Success:', values);
            };

            const onFinishFailed = errorInfo => {
                //console.log('Failed:', errorInfo);
            };

            return (
                <div className='MyContainer postBox'>
                <Form style={{direction:'rtl'}}
                {...layout}
                name="productForm"
                initialValues={{
                     name :props.product.name ,
                     price:props.product.price ,
                     details:props.product.caption


                    
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                >
                
                <Row>
                    <Col span={1} className='my-auto'>
    
                    <Form.Item {...tailLayout} name="isChosen"
                    
                    className="mt-10">
                            <Checkbox    checked={props.product.isChosen}/>
                    </Form.Item>

    
    
                    </Col>
                    <Col span={7}>
                        <img src={props.product.img} alt='post.img' className='mx-auto rounded-md w-9/12'/>
                    </Col>


                    <Col span={16}>
                        <Form.Item
                            label="نام کالا:"
                            name="name"
                            rules={[
                            {
                                required: true,
                                
                            },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="قیمت :"
                            name="price"
                            rules={[
                            {
                                required: true,
                            },
                            ]}
                        >
                            <InputNumber prefix='تومان'>
                            
                            
                            {props.price}
                            </InputNumber>
                        </Form.Item>
                            

                        <Form.Item {...tailLayout} 
                            label='توضیحات'
                            name='details'
                        >
                            <Input.TextArea>
                            {props.details}
                            </Input.TextArea>

                        </Form.Item>
                </Col>
                </Row>

                </Form>
                </div>
            );
            };

    export default proForm ;