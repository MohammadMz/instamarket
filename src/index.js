import React from 'react';
import ReactDOM from 'react-dom';
import './styles/app.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import '../node_modules/antd/dist/antd.css';
ReactDOM.render(
  <div>
    <App />
  </div>,
  document.getElementById('root')
);

serviceWorker.unregister();
