import React from 'react';
import './App.css';
import ShopName from './components/shopName/shopName';
import Main from './containers/Main/Main';
function App() {
  return (
    <div className='font-serif '>
    <React.Fragment>
      <Main/>
      <ShopName/>
    </React.Fragment>
    </div>
  );
}

export default App;
